\chapter{Hopcroft's Algorithm}
This section first introduces the concept of using partition refinement in DFAs, followed by an
overview of Moore's algorithm as it provides a solid basis for Hopcroft's algorithm. An overview is
then given on Hopcroft's algorithm. Lastly, the differences between Moore's algorithm and
Hopcroft's algorithm are highlighted, and a reasoning is provided that Hopcroft's algorithm is
correct, provided that Moore's algorithm is correct. This is done by individually considering the
correctness of all aforementioned differences.

\begin{figure}
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
		node distance=2.3cm,semithick]

		\node[initial,state]   (1)                     {1};
		\node[accepting,state] (2)  [above right of=1] {2};
		\node[accepting,state] (3)  [below right of=1] {3};
		\node[accepting,state] (4)  [above right of=2] {4};
		\node[state]           (5)  [below right of=2] {5};
		\node[accepting,state] (6)  [below right of=3] {6};
		\node[accepting,state] (7)  [right of=5]       {7};
		\node[state]           (8)  [right of=6]       {8};
		\node[state]           (9)  [right of=7]       {9};
		\node[state]           (10) [right of=9]       {10};

		\path
		(1)  edge                 node {a}   (2)
		     edge                 node {b}   (3)
		(2)  edge [bend left=10]  node {a}   (4)
		     edge                 node {b}   (5)
		(3)  edge [bend left=10]  node {a}   (6)
			 edge [bend left=10]  node {b}   (5)
		(4)  edge [loop above]    node {a}   (4)
			 edge [bend left=10]  node {b}   (2)
		(5)  edge                 node {a}   (7)
			 edge [bend left=10]  node {b}   (3)
		(6)  edge                 node {a}   (8)
			 edge [bend left=10]  node {b}   (3)
		(7)  edge                 node {a}   (4)
			 edge [bend left=10]  node {b}   (9)
		(8)  edge [loop below]    node {a,b} (8)
		(9)  edge [bend left=10]  node {a}   (7)
			 edge                 node {b}   (10)
		(10) edge                 node {a}   (8)
			 edge [loop above]    node {b}   (10);
	\end{tikzpicture}
	\caption{Automaton 1~\cite{gomez2013dfa}}
	\label{fig:CA1}
\end{figure}

\section{Partition Refinement in DFA Minimization}
Moore's and Hopcroft's algorithm share a very similar approach to automata
minimization~\cite{watson95}, utilizing equivalence relations, partition refinement and having a
top-down approach. Given an automaton $(\Sigma, S, s_0, \delta, F)$, the goal of the algorithms is
to create a partition $P$ of $S$ of which the associated equivalence relation is language
equivalence ($\sim$). Once this partition is created, the minimal DFA can be constructed using the
algorithm described in the preliminaries. In order to create the partition $S_\sim$, partition
refinement is used to gradually refine the coarsest possible non-trivial partition $\{S-F,F\}$
until it equals $S_\sim$.

\paragraph{Splitters}
A \textit{splitter} is a concept used in both Hopcroft's and Moore's algorithm. It is a tuple
$(S',l)$ of a set of states $S'$ and a letter $l$, that is used in refining a partition. In
particular, it is used to split a set $S$ of states into two new sets $S''$ and $S'''$. Moore's and
Hopcroft's algorithm do this by checking for every element in $S$, if it does or does not have a
transition to $S'$ with $l$.  All states that do are then separated from those that do not, such
that either $S'' = S' \cap \bigcup_{s' \in S'} \delta^{-1}(s',l)$ and $S''' = S' \ \bigcup_{s' \in
S'} \delta^{-1}(s',l)$, or $S'' = S' \ \bigcup_{s' \in S'} \delta^{-1}(s',l)$ and $S'' = S' \cap
\bigcup_{s' \in S'} \delta^{-1}(s',l)$ holds.

A set $S$ is \textit{split} if both $S''$ and $S'''$ are non-empty. We denote this as: $(S'', S''')
= S \mid (S',l)$. A special case is where a set $S$ forms a class in a partition $P$, we then say
that the set $S$ is \textit{split} in $P$ if the set is replaced in the partition with the two new
sets. Note that replacing a class in a partition as described does not violate any of the
properties of a partition.

\subsubsection{Moore's Algorithm}
Moore's algorithm was initially defined for Moore Machines~\cite{moore56}, but since DFAs are a
special case of Moore Machines, the algorithm is also applicable on them. I will first introduce a
general overview on the algorithm, and will then provide an elaborate example. The pseudocode
of the algorithm can be found in Algorithm~\ref{alg:moore}.
\begin{algorithm}
	\caption{Moore's Algorithm}
	\label{alg:moore}
	\begin{algorithmic}[1]
		\REQUIRE{$A := (\Sigma, S, s_0, \delta, F)$}
		\ENSURE{The minimized automaton with the same language as A}

		\STATE{$P = \{F, S-F\}$}
		
		\REPEAT
			\STATE{$P' = P$}
			\FORALL{$C \in P'$}
				\FORALL{$l \in \Sigma$}
					\FORALL{$C'$ split by $(C,l)$}
						\STATE{Split $C'$ in $P$}
					\ENDFOR
				\ENDFOR
			\ENDFOR
		\UNTIL{$P == P'$}
		\RETURN{a new DFA based on $A$ and $P$}
	\end{algorithmic}
\end{algorithm}

Let us start with a global overview of the algorithm. The algorithm starts with the aforementioned
coarse partition $P = \{F,S-F\}$. Every iteration of outermost for-loop Moore's algorithm stores
the current partition in a variable $P'$. This allows the partition that needs to be refined to be
destructively updated while still allowing access to the partition as it was before the iteration.

Every class of $P'$ is then used in combination with every letter to create a splitter $s$. This
splitter is then used to refine partition $P$ if possible. There are many implementations possible,
but the most typical way is through the inverse $\delta$-function outlined in the preliminaries.
First, it creates a set $I$ where $I = \bigcup_{s \in C} \delta^{-1}(s,l)$. Then, it splits every
class with an element (but not all) in I.  If, after a complete iteration over the classes of $P'$,
$P$ has not been updated, the algorithm converts the partition $P$ to a DFA. This DFA is then
returned.

Using this overview, let us consider the behaviour of Moore's algorithm when run on the
automata specified in Figure~\ref{fig:CA1}. The algorithm starts by creating the initial partition
$P$.

$$1,5,8,9,10 \mid 2,3,4,6,7$$

Once we reach Line 6 for the first time the internal state could be:

\begin{align*}
	P &= 1,5,8,9,10 \mid 2,3,4,6,7\\
	C &= 1,5,8,9,10\\
	l &= a\\
	P'&= 1,5,8,9,10 \mid 2,3,4,6,7\\
\end{align*}

Note that the internal state could also be different, given that every $C \in P'$ and $l \in
\Sigma$ need not be considered in any particular order.

On Line 6, the algorithm considers what classes are split with $(C,l)$. Like mentioned above, it
does so by creating a set $I$.

Consider the inverse $\delta$-function as applied on every element of $C$ with $a$.
\begin{align*}
	\delta^{-1}(1,a) &= \emptyset\\
	\delta^{-1}(5,a) &= \emptyset\\
	\delta^{-1}(8,a) &= \{6,8,10\}\\
	\delta^{-1}(9,a) &= \emptyset\\
	\delta^{-1}(10,a) &= \emptyset\\
\end{align*}

The union of all of these sets is $I$, we can see that $I = \{6,8,10\}$. This means that the first
class the first class is split as $1,5,9 \mid 8,10$ since $8$ and $10$ are in $I$ and $1,5,8$ are
not. The second class is split as $2,3,4,7 \mid 6$ such that $P = 1,5,8 \mid 8,10 \mid 2,3,4,7 \mid
6$.

The algorithm terminates when $P$ and $P'$ have not changed for an entire iteration of the ``repeat
until''. The continuation of the algorithm can be seen in Table \ref{tab:moore_example}. The table
shows the state as it is every time the algorithm reaches line 6. Iterations where $C$ consists of
a single state and no class is split are not shown.

\begin{longtable}{|c||c||c|}
	\hline
	$P$ & $C$ & $l$ \\\hline
	\multicolumn{3}{|c|}{$C' = 1,5,8,9,10 \mid 2,3,4,6,7$}\\\hline\hline
	$1,5,8,9,10 \mid 2,3,4,6,7$ & $1,5,8,9,10$ & $a$ \\\hline
	$1,5,9 \mid 8,10 \mid 6 \mid 2,3,4,7$ & $1,5,8,9,10$ & $b$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2,3,7 \mid 4$ & $2,3,4,6,7$ & $a$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2,3,7 \mid 4$ & $2,3,4,6,7$ & $b$ \\\hline\hline
	\multicolumn{3}{|c|}{$C' = 1,5 \mid 9 \mid 8,10 \mid 6 \mid 2,3,7 \mid 4$}\\\hline\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2,3,7 \mid 4$ & $1,5$ & $a$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2,3,7 \mid 4$ & $1,5$ & $b$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2,3 \mid 7 \mid 4$ & $8,10$ & $a$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2,3 \mid 7 \mid 4$ & $8,10$ & $b$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2,3 \mid 7 \mid 4$ & $6$ & $a$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$ & $6$ & $b$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$ & $2,3,7$ & $a$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$ & $2,3,7$ & $b$ \\\hline\hline
	\multicolumn{3}{|c|}{$C' = 1,5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$}\\\hline\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$ & $1,5$ & $a$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$ & $1,5$ & $b$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$ & $8,10$ & $a$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$ & $8,10$ & $b$ \\\hline
	$1,5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$ & $2$ & $a$ \\\hline
	$1 \mid 5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$ & $2$ & $b$ \\\hline\hline
	\multicolumn{3}{|c|}{$C' = 1 \mid 5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$}\\\hline\hline
	$1 \mid 5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$ & $8,10$ & $a$ \\\hline
	$1 \mid 5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$ & $8,10$ & $b$ \\\hline\hline
	\multicolumn{3}{|c|}{$C' = 1 \mid 5 \mid 9 \mid 8,10 \mid 6 \mid 2 \mid 3 \mid 7 \mid 4$}\\\hline
\caption{Moore's algorithm as applied on Automata \ref{fig:CA1}}
\label{tab:moore_example}
\end{longtable}

\section{Overview}
\begin{algorithm}
	\caption{Hopcroft's Algorithm}
	\label{alg:hopcroft}
	\begin{algorithmic}[1]
		\REQUIRE{$A := (\Sigma, S, s_0, \delta, F)$}
		\ENSURE{The minimized automaton with the same language as A}

		\STATE{$P = \{F, S-F\}$}
		\STATE{$M = min(F, S-F)$ (the class with the least amount of states)}
		\STATE{$\mathcal{W} = \emptyset$}

		\FORALL{$l \in \Sigma$}
			\STATE{$\mathcal{W}.push((M,l))$}
		\ENDFOR
		\WHILE{$\mathcal{W} \neq \emptyset$}
			\STATE{$(S,l) = \mathcal{W}.pop()$}
			\FORALL{$C \in P$ for which $C$ is split by $(S,l)$}
				\STATE{$(C', C'') = c \mid (S,l)$}
				\STATE{Replace $C$ for $C'$ and $C''$ in $P$}

				\STATE{$C''' = min(C', C'')$}

				\FORALL{$x \in \Sigma$}

					\FORALL{$(C,x) \in \mathcal{W}$ (where $C$ is the same $C$ as above)}
						\STATE{Replace $(C,x)$ with $(C',x)$ and $(C'',x)$}
					\ENDFOR

					\STATE{$\mathcal{W}.push((C''',x))$}
				\ENDFOR
			\ENDFOR
		\ENDWHILE
		\RETURN{a new DFA based on $A$ and $P$}
	\end{algorithmic}
\end{algorithm}
Hopcroft first introduced his minimization algorithm in his 1971 paper \textit{An n log n algorithm
for minimizing states in a finite automaton}~\cite{hopcroft71}. The title of this paper suggests
that his algorithm has an $O(n \log n)$ complexity (with $n$ the number of states). This is true, but
only for a constant size alphabet. If we incorporate $k$ for the size of the alphabet, we get $O(kn
\log n)$, which is still a good improvement over the complexity of Moore's algorithm ($O(kn^2)$).

While Hopcroft does not reference Moore's algorithm, it can be said that Hopcroft's algorithm
borrows many ideas from Moore. Mainly, choosing the initial partition, and refining until it
represents the partition based on language equivalence. It improves on these ideas in several ways,
but the most significant way is the \textit{waiting set}. The waiting set is, in essence, a set of
splitter that have yet to be considered for splitting classes in the partition. I will go more in
detail, after the overview.

In this part, I first provide a global overview of the algorithm using the pseudocode in
Algorithm \ref{alg:hopcroft}, and continue to provide the reasoning I mentioned before.

\paragraph{Line 1} creates the initial partition.

\paragraph{Line 2} does preparational work for the loop on Line 4--6.

\paragraph{Line 3} initiates the waiting set. The waiting set is used to store a set of
"splitters". Splitters are used to split the partitions that are in $P$ at any given time.

\paragraph{Line 4--6} adds the smallest class to the waiting set.

\paragraph{Line 7} checks if the waiting set is empty. Since the splitting of a partition will
always result in pushing to the waiting set (Line 17). It is trivial to realize that the algorithm
must terminate. Every iteration one element is removed from the waiting set, and elements are only
added when an partition is split. Thus, the waiting set will always reach an empty state once every
partition is split.

\paragraph{Line 8} pops retrieves one element from the waiting set. This element is used in the
splitting process on Line 9--18.

\paragraph{Line 9} loops over every class that is split by splitter $(S,l)$.

\paragraph{Line 10-11} splits every class $C$ that is split by $(S,l)$, $C$ is then replaced with
the results of the split ($c'$ and $c''$).

\paragraph{Line 12} selects the smallest of the two new classes to be added to the waiting set.

\paragraph{Line 14-16} Ensures that every class that was split in $P$ is also split in the waiting
set.

\paragraph{Line 17} adds only the smallest class to the waiting set.

\section{Hopcroft's algorithm compared to Moore's algorithm}
As mentioned before, Hopcroft's algorithm is very similar to that of Moore. However, there are some
differences that decrease the complexity. I will now consider the differences with Moore's algorithm
one by one.

\subsubsection{Queue}
Hopcroft realized that it is not needed to split based on the same splitter twice.

Consider the splitter $(C,l)$ that is used to split some class $C'$ into $C''$ and $C'''$. We
will only consider $C''$ in the rest of this paragraph, but it trivially also holds for $C'''$.
Since $C'$ was split by $(C,l)$, every state in $C''$ either has a transition to $C$, or none
of the states do.

Since we consider every class that is split by $(C,l)$, the resulting partition only has classes
for which either all states transition to a state in $C$ with $l$, or none. Splitting a class in a
later stage will not compromise this property. As such, it is not needed to split on $(C,l)$ again.

In order to incorporate this in the algorithm, Hopcroft uses some waiting set of splitters. What
type of waiting set is used (Queue/Stack) is not specified in Hopcroft's 1971 paper. Research
shows, however, that a FILO implementation is desired~\cite{baclet2006around,puaun2008hopcroft}.

\subsubsection{The Smallest Class}
Every time a class is split into two, the smallest of the two resulting classes is added to the
waiting set.  Since we must at some point use the $\delta^{-1}$-function on every state in a
splitter, using the smallest possibly splitters is preferred. It can be shown that only one of the
two classes needs to be considered. Please note that the following considers only a single letter
$l$, but that it holds for every letter in the alphabet.

Let $C$ be a class that is split into $C'$ and $C''$. there are two distinct cases: either $(C,l)$
is in the waiting set, or it is not.
	
In the first case, both $(C',l)$ and $(C'',l)$ will end up in the waiting set due to the split.
Given that the waiting set is indeed a set, adding one or the other will not change the contents of
the set.

In the second case, another distinction must be made. Either it was in the waiting set at an
earlier stage, or it was never in the waiting set. In the first case, we have already split based on
$(C,l)$. This, in turn, implies that for every class all states either have transitions that go to
$C$ via $l$, or none. When all states have such a transition, we can only split further based on
whether states transition to $C'$ or $C''$ with $l$. Since that is the only distinction that can be
made, it is easy to realize that we can split on either $(C',l)$ or $(C'',l)$.

If no transitions go to $C$ with $l$, there is nothing that can be gained from splitting on
either $C'$ or $C''$. So it does not matter which we add.

Consider Figure~\ref{fig:smallest_class} where $D$ is some class that is used to represent all
other classes in the example above. The first figure shows the classes where we have not yet split
$C$. The second figure shows the classes as it would be after splitting on $(C,l)$, and the last
figure shows the classes after $C$ is split.

\begin{figure}
	\centering
	\begin{tikzpicture}[->,node distance=2.5cm]
		\path
		(2.6,0.25) edge (0,0.5)
		(2.4,0.4) edge (0.2,-0.5)
		(2.5,0.8) edge (-0.2,-1);
		\node [ellipse,draw,minimum height=3cm,minimum width=1cm] (C) [] {$C$};
		\node [ellipse,draw,minimum height=3cm,minimum width=1cm] (D) [right of = C] {$D$};
	\end{tikzpicture}\\
	\begin{tikzpicture}[->,node distance=2.5cm]
		\path
		(2.6,0.25) edge (0,0.5)
		(2.4,0.4) edge (0.2,-0.5)
		(2.5,0.8) edge (-0.2,-1);
		\node [ellipse,draw,minimum height=3cm,minimum width=1cm] (C) [] {$C$};
		\node [ellipse split,draw,minimum height=3cm,minimum width=1cm] (D) [right of = C] {$D'$ \nodepart{lower}
		$D''$};
	\end{tikzpicture}\\
	\begin{tikzpicture}[->,node distance=2.5cm]
		\path
		(2.6,0.25) edge (0,0.5)
		(2.4,0.4) edge (0.2,-0.5)
		(2.5,0.8) edge (-0.2,-1);
		\node [ellipse split,draw,minimum height=3cm,minimum width=1cm] (C) [] {$C'$ \nodepart{lower} $C''$};
		\node [ellipse split,draw,minimum height=3cm,minimum width=1cm] (D) [right of = C] {$D'$ \nodepart{lower}
		$D''$};
	\end{tikzpicture}
	\caption{Smallest class}
	\label{fig:smallest_class}
\end{figure}

In the second case ($(C,l)$ was never in the waiting set), we know that the its smaller counterpart
$C_2$ (the smaller class that at some point formed a single class with this class) was added to the
waiting set instead.
We assume in the next paragraph that the order in which the splitters are considered does not impact the result of the algoritm. This is true, but not proven in this thesis.

We know that every class has only transitions to $C_2 \cup C$ with $l$, or none. Given the above we can assume that $(C_2,l)$ is the the splitter that is considered first. $(C_2,l)$ will split every class such that every class has only transitions to $C_2$ with $l$, $C$ with $l$, or neither. Since we can only every split classes with transitions to $C$, it does not matter if we split on $C'$ or $C''$.

\section{Example}
Let's consider Hopcroft's algorithm as it would be run on the automaton in Figure~\ref{fig:CA1}.
The following table shows the inner state every time a splitter is popped from the waiting set.

\begin{longtable}{|l|c|c|}
	\hline
	$0$ & $P$         & $1,5,8,9,10 \mid 2,3,4,6,7$\\\cline{2-3}
		& $(S,l)$       & \\\cline{2-3}
	    & $\mathcal{W}$ & $(1,5,8,9,10;a), (1,5,8,9,10;b)$\\\hline\hline
	$1$ & $P$         & $1,5,9 \mid 8,10 \mid 2,3,4,7 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(1,5,8,9,10;a)$\\\cline{2-3}
		& $\mathcal{W}$ & $(1,5,9;b), (8,10;b), (8,10;a), (6;a), (6;b)$\\\hline\hline
	$2$ & $P$         & $1,5,9 \mid 8,10 \mid 2,3,7 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(1,5,9;b)$\\\cline{2-3}
		& $\mathcal{W}$ & $(8,10;b), (8,10;a), (6;a), (6;b), (4;a), (4;b)$\\\hline\hline
	$3$ & $P$         & $1,5 \mid 9 \mid 8,10 \mid 2,3,7 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(8,10;b)$\\\cline{2-3}
		& $\mathcal{W}$ & $(8,10;a), (6;a), (6;b), (4;a), (4;b), (9;a), (9;b)$\\\hline\hline
	$4$ & $P$         & $1,5 \mid 9 \mid 8,10 \mid 2,3,7 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(8,10;a)$\\\cline{2-3}
		& $\mathcal{W}$ & $(6;a), (6;b), (4;a), (4;b), (9;a), (9;b)$\\\hline\hline
	$5$ & $P$         & $1,5 \mid 9 \mid 8,10 \mid 2,7 \mid 3 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(6;a)$\\\cline{2-3}
		& $\mathcal{W}$ & $(6;b), (4;a), (4;b), (9;a), (9;b), (3;a), (3;b)$\\\hline\hline
	$6$ & $P$         & $1,5 \mid 9 \mid 8,10 \mid 2,7 \mid 3 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(6;b)$\\\cline{2-3}
		& $\mathcal{W}$ & $(4;a), (4;b), (9;a), (9;b), (3;a), (3;b)$\\\hline\hline
	$7$ & $P$         & $1,5 \mid 9 \mid 8,10 \mid 2,7 \mid 3 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(4;a)$\\\cline{2-3}
		& $\mathcal{W}$ & $(4;b), (9;a), (9;b), (3;a), (3;b)$\\\hline\hline
	$8$ & $P$         & $1,5 \mid 9 \mid 8,10 \mid 2,7 \mid 3 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(4;b)$\\\cline{2-3}
		& $\mathcal{W}$ & $(9;a), (9;b), (3;a), (3;b)$\\\hline\hline
	$8$ & $P$         & $1,5 \mid 9 \mid 8,10 \mid 2,7 \mid 3 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(9;a)$\\\cline{2-3}
		& $\mathcal{W}$ & $(9;b), (3;a), (3;b)$\\\hline\hline
	$9$ & $P$         & $1,5 \mid 9 \mid 8,10 \mid 2 \mid 7 \mid 3 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(9;b)$\\\cline{2-3}
		& $\mathcal{W}$ & $(3;a), (3;b), (2;a), (2;b)$\\\hline\hline
	$10$& $P$         & $1,5 \mid 9 \mid 8,10 \mid 2 \mid 7 \mid 3 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(3;a)$\\\cline{2-3}
		& $\mathcal{W}$ & $(3;b), (2;a), (2;b)$\\\hline\hline
	$11$& $P$         & $1,5 \mid 9 \mid 8,10 \mid 2 \mid 7 \mid 3 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(3;b)$\\\cline{2-3}
		& $\mathcal{W}$ & $(2;a), (2;b)$\\\hline\hline
	$12$& $P$         & $1 \mid 5 \mid 9 \mid 8,10 \mid 2 \mid 7 \mid 3 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(2;a)$\\\cline{2-3}
		& $\mathcal{W}$ & $(2;b), (1;a), (1;b)$\\\hline\hline
	$13$& $P$         & $1 \mid 5 \mid 9 \mid 8,10 \mid 2 \mid 7 \mid 3 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(2;b)$\\\cline{2-3}
		& $\mathcal{W}$ & $(1;a), (1;b)$\\\hline\hline
	$14$& $P$         & $1 \mid 5 \mid 9 \mid 8,10 \mid 2 \mid 7 \mid 3 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(1;a)$\\\cline{2-3}
		& $\mathcal{W}$ & $(1;b)$\\\hline\hline
	$15$& $P$         & $1 \mid 5 \mid 9 \mid 8,10 \mid 2 \mid 7 \mid 3 \mid 4 \mid 6$\\\cline{2-3}
	    & $(S,l)$       & $(1;b)$\\\cline{2-3}
		& $\mathcal{W}$ & $(1;b)$\\\hline
		\caption{Hopcroft as run on the automaton in Figure~\ref{fig:CA1}}
\end{longtable}

\section{Implementation}
The algorithms in this thesis were implemented in C++. This section will highlight key parts of the
algorithm, and consider their implementation in C++.

\subsubsection{$\delta^{-1}$ over $\delta$}
Line 9 of Hopcroft's algorithm requires determining which of the classes are split by the
splitter. In order to do this in the least amount of time possible, we make use of the inverse delta
function: $\delta^{-1} : S \times \Sigma \rightarrow \mathcal{P}(S)$. This function takes some
state and letter, and returns the states that reach that state with that letter, as defined in the
preliminaries. In order to allow this function to be performed in $O(1)$, the inverse transition
table is calculated before Line 9 is reached.
\begin{lstlisting}
for (DFAState* state : dfa->states) {
	state->reverse_transitions.resize(state->transitions.size());
}
for (DFAState* state : dfa->states) {
	for (unsigned int i = 0; i < state->transitions.size(); i++) {
		state->transitions[i]->reverse_transitions[i].push_back(state);
	}
}
\end{lstlisting}

\subsubsection{Class to Splitter map}
An optimal implementation of line 14 in the algorithm requires an $O(1)$ mapping between a class and
the splitters that are based on this partition.

\begin{lstlisting}
class Class {
	public:
		std::list<DFAState*> states;
		(*@\makebox[\linewidth][l]{$\smash{\vdots}$}@*)
		std::list<Splitter*> splitters;
};
\end{lstlisting}

Besides classes knowing all splitters that are based on it, the opposite is also true.
\begin{lstlisting}
class Splitter {
	public:
		Class* c;
		unsigned int letter;
};
\end{lstlisting}
The class in the splitters enables Line 15 to be optimally implemented. There is no need to
split class and splitters separately. Splitting the class means that the splitter is also
split. Of course, the other resulting class still needs to be added to $\mathcal{W}$.

Achieving high performance on Line 9 is done in a similar manner. Every class consists of a set
of states, every state has a pointer to the class to which is belongs. The combination of this
mechanic enables us to consider only class that could potentially be split, ignoring the others.
\begin{lstlisting}
for (DFAState* s : splitter->p->states) {
	for (DFAState* orig_state : s->reverse_transitions[splitter->letter]) {
		if (orig_state && !(orig_state->different_part)) {
			orig_state->different_part = true;
			Class* temp = orig_state->p;
			if (temp->times_added == 0) {
				result.push_back(temp);
			}
			temp->times_added++;
		}
	}
}
\end{lstlisting}
The \lstinline|times_added| variable is later used to determine if every every/no state had
transitions to the splitter, if this is case, the class as a whole is not split.

\section{Complexity of Implementation}
The focus of this paper lies not with a formal analysis of the complexity of the implementation.
Instead, I would like to refer to an unpublished paper by Hang Zhou on implementing Hopcroft's
algorithm~\cite{zhou2009implementation}. Which describes that datastructures that are needed for an
optimal implementation.
