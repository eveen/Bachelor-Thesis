\chapter{Preliminaries}\label{preliminaries}
\section{Deterministic Finite Automata}
In order to present the automata minimizing algorithms in this thesis, it is useful to introduce
a consistent notation and basic principles of automata minimization. This section serves to
provide both.

A Deterministic Finite Automaton (DFA) is a 5-tuple $(\Sigma, S, s_0, \delta, F)$ where:
\begin{description}[align=right,labelwidth=3cm]
	\item[$\Sigma$] is an \textit{alphabet}; a finite set of \textit{letters}
	\item[$S$] is the finite set of states
	\item[$s_0$] is the initial state such that $s_0 \in S$
	\item[$\delta$] $:S \times \Sigma \rightarrow S$ is the transition function
	\item[$F$] is the set of accepting states such that $F \subseteq S$
\end{description}

A \textit{word} is a finite \textit{string} of letters, where a string is an ordered list of
letters.  The empty word is denoted as $\epsilon$.  Given an alphabet $\Sigma$, the set of all
possible words is $\Sigma^*$. The \textit{$\delta$-function} (transition function) is a binary
function that maps some letter and a state to some state. A state $s$ \textit{transitions} to $s'$
with letter $l$ if $\delta(s,l) = s'$. 

The $\delta$-function is overloaded on words such that the signature changes to $\delta : S \times
\Sigma^* \rightarrow S$ where it represents the state that the $\delta$ function results when it is
applied to every letter in a word consecutively. This is recursively defined as:
\begin{align*}
	\delta(s, \epsilon) &= s\\
	\delta(s, lw) &= \delta(\delta(s,l), w)
\end{align*}

The \textit{inversed $\delta$-function} ($\delta^{-1} : S \times \Sigma \rightarrow
\mathcal{P}(S)$) is also defined. Given a state $s$ and a letter $l$, $\delta^{-1}$ serves to
return a set of states $S'$ that contains all states that transition to $s$ with $l$:
$\delta^{-1}(s,l) = \{ s' \mid \delta(s',l) = s \}$.

A state $s$ in an automaton $A := (\Sigma, S, s_0, \delta, F)$ is said to \textit{accept} a word $w$
if $\delta(s, w) \in F$. Every state $s$ in automaton $A$ has a language $\mathcal{L}(A,s)$ defined
as: $$\mathcal{L}(A,s) = \{w \in A^* \mid \delta(A,s) \in F\}$$

The language $\mathcal{L}$ of an automaton $A$ is the set of words that is accepted by the
initial state of the automaton: $\mathcal{L}(A,s_0)$. We denote $\mathcal{L}(A,s_0)$ by
$\mathcal{L}(A)$.

A \textit{minimal} DFA $A$ with states $S$ is such that there is no other DFA $A'$ containing
states $S'$ where $\mathcal{L}(A) = \mathcal{L}(A')$ and $|S'| \leq |S|$. Note that a minimal
automaton $A$ of some language $\mathcal{L}(A)$ is unique in the way that there are no other
automaton that accepts the same language with the same amount of
states~\cite{hopcroft2001introduction}.

\section{Non-deterministic Finite Automata}
A Non-deterministic Finite Automaton (NFA) is an extension of notion of DFAs that, in essence,
extends the $\delta$-function. Where a DFA has the $\delta$-function defined as $\delta : S \times
\Sigma \rightarrow S$, an NFA has it defined as $\delta : S \times \Sigma \rightarrow
\mathcal{P}(S)$, this enables a single state to transition to multiple states with a single letter.
Furthermore, a common extension is to also allow multiple initial states. As such an NFA is defined
as $(\Sigma, S, I, \delta, F)$ where $\delta$ is the aforementioned function, and $I$ is the set of
initial states where $I \subseteq S$. The rest of the five-tuple remains as it was.

We need to update our definition of languages to match with the new type of the $\delta$-function.
For an NFA $A$, the language is defined as: $\mathcal{L}(A) = \{w \mid \exists_{s \in I}[\delta(s,w)
\in F]\}$.

\section{Determinization}
Let $A$ be an NFA, the act of determinization is constructing a DFA $A'$ such that $\mathcal{L}(A) =
\mathcal{L}(A')$. A typical method of doing this is through ``Subset construction''. The intuitive
idea is that we represent a set of NFA States as a single DFA state. The pseudocode clarifies:

\begin{algorithm}
	\caption{Subset Construction}
	\label{alg:subset}
	\begin{algorithmic}[1]
		\REQUIRE{$NFA:=(\Sigma, S, I, \delta, F)$}
		\ENSURE{$DFA:=(\Sigma, S', s_0', \delta', F')$ such that $\mathcal{L}(NFA) =
		\mathcal{L}(DFA)$}

		\STATE{Create the initial states of the DFA that represents $I$}
		\FOR{the newly created DFA State}
			\FORALL{$a \in \Sigma$}
				\STATE{Apply $a$ on the state to obtain a new set of states}
				\STATE{This set is a (new) DFA state}
			\ENDFOR
			\STATE{Apply step 2 to every new DFA state}
		\ENDFOR
	\end{algorithmic}
\end{algorithm}

Consider the NFA in Figure~\ref{fig:BZ1} on page~\pageref{fig:BZ1}. Applying subset construction
yields the automaton in Figure \ref{fig:BZ2}. For clarity, the DFA states are labeled with the NFA
states that they represent.

\section{Reversal}
Let $l_0l_1l_2\dots l_{i-1}l_i$ be some word $w$ where $l_i$ is the i'th letter of a word. The
\textit{reversed} word $w^R$ is then defined as the word where all letters are in reverse order:
$l_il_{i-1}\dots l_2l_1l_0$.

The reversed language $\mathcal{L}^R$ of some language $\mathcal{L}$ is the
language where all words are reversed: $$\forall_{w \in \Sigma^*} [w \in
\mathcal{L} \Leftrightarrow w^R \in \mathcal{L}^R]$$

Given a DFA (or NFA) $A$, a NFA $A'$ can be constructed that accepts the reverse language. When
$A'$ is constructed, we say that $A$ is \textit{reversed}. A DFA can be reversed using the
following algorithm:
\begin{algorithm}
	\caption{DFA Reversal}
	\label{alg:reversal}
	\begin{algorithmic}[1]
		\REQUIRE{$DFA:=(\Sigma, S, s_0, \delta, F)$}
		\ENSURE{$NFA:=(\Sigma, S, I, \delta', F')$ where $\mathcal{L}(NFA) = \mathcal{L}(DFA)^R$}

		\STATE{Reverse the delta function, iff: $\delta(s, a) = s'$, then $s \in \delta'(s', a)$ for
		every $S \times l$}. This can also be written as: $\delta' := \delta^{-1}$.
		\STATE{$I = F$}
		\STATE{$F' = \{s_0\}$}
	\end{algorithmic}
\end{algorithm}

\section{Equivalence Classes}
To understand the notion of an equivalence class, one must first understand what an equivalence
relation is, which in turn requires understanding of relations. A \textit{relation} $R$ is a set of
tuples over a set $X$ ($R \subseteq X \times X$). An \textit{equivalence relation} is a relation
that is binary, reflexive, symmetric and transitive.

The properties of an equivalence relation ensure that within a domain, subsets must arise for which
all elements are in the equivalence relation with each other. Such groups are equivalence classes.

\begin{definition}
Let $R$ be an equivalence relation, and $x$ be some element. The equivalence class of $x$ with
	relation $R$ is then defined as:
	$$[x] = \{y \mid x R y\}.$$
\end{definition}

\begin{definition}
	A partition $P$ of some set $X$, is defined by the following properties:
	\begin{itemize}
		\item $P \subset \mathcal{P}(X)$
		\item $\forall_{C,C' \in P}[C \neq C' \rightarrow C \cap C' = \emptyset]$
		\item $\bigcup_{C \in P} = S$
	\end{itemize}
\end{definition}

The number of classes that a partition $P$ contains is denoted as $|P|$.

Since no element of a set $X$ can be in two different equivalence classes with the same relation
$R$, the equivalence classes of $R$ define a partition $P$ of $X$. This is denoted as $P = X_R$.
Conversely, any Partition $P$ on $X$ defines an equivalence relation $R$.

These concepts can be applied to automata theory. Consider some automaton $(\Sigma, S, s_0, \delta,
F)$. In order to create the aforementioned equivalence classes on states, the equivalence relation
must first be defined. We use ``language equivalence'' for this purpose. If two states have the same
language, they are ``language equivalent'', and therefore in the same equivalence class.

\begin{definition}
	Two states $s$, $s'$ in an automata $A$ are language equivalent iff:
	$$\mathcal{L}(A,s) = \mathcal{L}(A,s')$$
\end{definition}

This thesis uses the notation $s \sim s'$ to denote that that $s$ and $s'$ are language
equivalent.

Consider the automaton in Figure~\ref{fig:eq_class_example}. The first state has the language that
is represented by the regular expression $(a|b)a^*b(a|b)^*$, it is the only state that has this
language, and is therefore in an equivalence class of its own. Another equivalence class contains
state 2 and 3, both having the language represented by $a^*b(a|b)^*$. State 4 is in the last
equivalence class. The complete partition is: $\{\{1\},\{2,3\},\{4\}\}$. For readability the rest
of this thesis will use the following notation: $1 \mid 2,3 \mid 4$.

\begin{figure}
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
		node distance=2.3cm,semithick]

		\node[initial,state]   (1)                     {1};
		\node[state]           (2)  [above right of=1] {2};
		\node[state]           (3)  [below right of=1] {3};
		\node[accepting,state] (4)  [above right of=3] {4};

		\path
		(1)  edge                 node {a}   (2)
		     edge                 node {b}   (3)
		(2)  edge [loop above]    node {a}   (2)
		     edge                 node {b}   (4)
		(3)  edge [loop below]    node {a}   (3)
		     edge                 node {b}   (4)
		(4)  edge [loop above]    node {a,b} (3);
	\end{tikzpicture}
	\caption{Small Example Automaton}
	\label{fig:eq_class_example}
\end{figure}

\section{Partitions to DFA}
\begin{definition}
	Let $A := (\Sigma, S, s_0, \delta, F)$ be some DFA and $P$ be the partition $S_{\sim}$. We can
	then construct the new (minimal) automaton $A'$ as $(\Sigma, S', s_0', \delta', F')$, where:
	\begin{description}
		\item[$S'$] is $P$
		\item[$s_0'$] is the class $C \in P$ for which $s_0 \in C$
		\item[$\delta'$] $: S' \times \Sigma \rightarrow S'$ such that 
			$\forall_{s \in S, l \in \Sigma}[\delta'([s],l) = [\delta(s,l)]]$
		\item[$F'$] be a set such that
			$$\forall_{C \in P}[\exists_{s \in C}[s \in F] \Leftrightarrow C \in F']$$
	\end{description}
\end{definition}

When a partition based on language equivalence is created for some automaton $A$, it can be used to
construct a minimal DFA $A'$ where $\mathcal{L}(A) = \mathcal{L}(A')$. The idea rests on the fact
that a state $s$ with transition $\delta(s,a) = s'$ could instead have a transition to any other
state in the equivalence class of $s'$. As such, we can create a new DFA where we consider every
class in the partition to be a single state.

\begin{proof}
	Let $A$ be some automaton with $s$ and $s'$ being states in that automaton. Let $C$ be the
	equivalence class based on $~$ that contains at least $s'$ and $s''$. Given the transition
	function $\delta(s,a) = s'$ it is trivial that $\{a\} \cdot \mathcal{L}(A,s') \subseteq
	\mathcal{L}(A,s)$ where $\cdot$ is the concatenation function. If we now assume that
	$\delta(s,a) = s''$ instead, we can trivially say that $\{a\} \cdot \mathcal{l}(s,s') = \{a\}
	\cdot \mathcal{l}(s,s'')$. Given this, it is shown that $\{a\} \cdot \mathcal{L}(A,s'')
	\subseteq \mathcal{L}(A,s)$. As such, we can conclude that any transition $\delta(s, a) = s'$
	can be replaced by $\delta(s, a) = s''$ if $s'$ and $s''$ are in the same equivalence class.
	Which, in turn, shows that equivalence classes can be considered as single states without
	changing the language of the automaton.
\end{proof}

Consider the automaton in Figure~\ref{fig:eq_class_example} with partition $1 \mid 2,3 \mid 4$ as
an example. Following the definitions above a new automaton $(\Sigma, S, s_0, \delta, F)$ would be
defined as such:

\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
	node distance=2.3cm,semithick]

	\node[initial,state]   (1)                     {$\{1\}$};
	\node[state]           (2)  [right of=1]       {$\{2,3\}$};
	\node[accepting,state] (3)  [right of=2]       {$\{4\}$};

	\path
	(1)  edge                 node {a,b} (2)
	(2)  edge [loop above]    node {a}   (2)
		 edge                 node {b}   (3)
	(3)  edge [loop above]    node {a,b} (3);
\end{tikzpicture}

When a DFA is created in such a way, it is guaranteed to be the minimal automaton.

\section{Partition Refinement}
Given a partition $P$, a refined partition $P'$ is such that the following holds:

\begin{itemize}
	\item $\bigcup_{C \in P} = \bigcup_{C' \in P'}$
	\item $|P| < |P'|$
	\item $\forall_{C' \in P'}[\exists_{C \in P}[C' \subseteq C]]$
\end{itemize}

In this case, $P'$ is a \textit{refinement} of $P$, vice versa, $P$ is \textit{coarser} than $P'$.
