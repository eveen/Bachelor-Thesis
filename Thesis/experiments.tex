\chapter{Experiments}
\section{Code}
The implementations used in the experiments are written in C++14. They use the aforementioned
techniques to create the most optimal versions of the algorithms. The implementations, including
the Random Automata Generator written in Clean can be found in the git
repository~\footnote{gitlab.science.ru.nl/eveen/Bachelor-Thesis}.

\section{Benchmark Set}
A benchmark is needed to determine the performance of the algorithms. In order to ensure that their
practical performance is tested, it is needed that the automata in the benchmark are from practical
applications. Random automata might not have similar characteristics to the automata that are used
in practice. For this reason, a benchmark set originating from model checking is used (see
subsection~\ref{sec:model_automata}). In order to provide contrast, and address a possible issue
regarding a characteristic of the automata, random
automata were also generated (see subsection~\ref{sec:rand_automata}).

\subsection{Model Checking Automata}\label{sec:model_automata}
The automata used in this thesis were generated from the intermediate steps of model
checking~\cite{abdulla2010simulation}, with the purpose of benchmarking.

The paper for which they were created is not the only paper in which they were used in
benchmarking. In particular, they have been used to benchmark an optimization of the algorithm by
Hopcroft and Karp, using a technique called \textit{bisimulation up to
congruence}~\cite{bonchi2013checking}. In total, there are 1604 automata in the benchmark set.

The set has been incorporated into the ``automatark''\footnote{github.com/lorisdanto/automatark}
benchmark set, which contains several types of automata that were all aggregated with the purpose
of benchmarking.

Due to the origin of the automata, they have some characteristics that might not be ideal in the
case of comparing the performance of the algorithms implemented in this thesis. In particular, one
such characteristic is that they have no cycles that contain more than one state.  That is, all
automata look somewhat like the automaton in Figure~\ref{fig:benchmark_example}. 
\begin{figure}
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
		node distance=2cm,semithick]

		\node[initial,state]   (0)                     {0};
		\node[state]           (1)  [right of=0]       {1};
		\node[state]           (2)  [right of=1]       {2};
		\node[state]           (3)  [above right of=2] {3};
		\node[state]           (4)  [right of=3]       {4};
		\node[state]           (5)  [right of=2]       {5};
		\node[state]           (6)  [below right of=2] {6};
		\node[state]           (7)  [right of=5]       {7};
		\node[accepting,state] (8)  [right of=7]       {8};

		\path
		(0)  edge                 node {a,b} (1)
		(1)  edge [loop above]    node {a}   (1)
		     edge                 node {b}   (2)
		(2)  edge                 node {b}   (3)
		     edge                 node {b}   (5)
		     edge                 node {a}   (6)
		(3)  edge [loop above]    node {b}   (3)
		     edge                 node {a}   (4)
		(4)  edge                 node {a}   (7)
		(5)  edge                 node {b}   (7)
		(6)  edge [bend right=20] node {a}   (7)
		(7)  edge                 node {a,b} (8);
	\end{tikzpicture}
	\caption{Benchmark Automaton 1}
	\label{fig:benchmark_example}
\end{figure}

The fact that the automata have no cycles consisting of more than 1 state could negatively
influence the balance of the benchmark, especially considering the similarity of these automata
compared to Brzozowski's algorithm's worst case automaton shown in
Figure~\ref{fig:broz_worst_case}.

Additionally, all automata are NFAs rather than DFAs (that would both work with Hopcroft's
algorithm and Brzozowski's algorithm). In order to more accurately benchmark the algorithms, all
NFAs are first converted to DFAs.

\begin{figure}
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
		node distance=2cm,semithick]

		\node[initial,state]   (0)                     {0};
		\node[state]           (1)  [right of=0]       {1};
		\node[state]           (2)  [right of=1]       {2};
		\node[state]           (3)  [right of=2]       {3};
		\node[state]           (4)  [right of=3]       {$n-1$};
		\node[accepting,state] (5)  [right of=4]       {$n$};

		\path
		(0)  edge                 node {a} (1)
		(1)  edge                 node {a} (2)
		(2)  edge                 node {a} (3)
		(3)  edge [dashed]        node {a} (4)
		(4)  edge                 node {a} (5)
		(5)  edge [loop above]    node {a} (5);
	\end{tikzpicture}
	\caption{Brzozowski's algorithm Worst Case}
	\label{fig:broz_worst_case}
\end{figure}

\subsection{Random Automata}\label{sec:rand_automata}
In order to allow for a more balanced benchmark, random automata were generated using a modified
version (in Clean) of the generator used by Bonchi and Pous~\cite{bonchi2013checking}. I used the
following variables to generate the automata: a linear transition density of 1.25 (every state has
an expected out-degree of 1.25 for every letter) as proposed by Tabakov and
Vardi~\cite{tabakov2005experimental}; a 0.1 chance of a state being an initial state; and a 0.1
chance of state being an accepting state. State 1, is always both an initial state, and an
accepting state. The following code is used to generate the automata:

\lstset{style=clean}

\begin{lstlisting}
random_nfa :: Int Int Real Real Real [Real] -> NFA
random_nfa n v r pa pi randReals
# d = r / (toReal n)
# randDeltas = diag3 [1 .. n] [1 .. v] [1 .. n]
# randDeltas = dropOnChance randDeltas randReals d
# randReals = drop (n * n * v) randReals
# accepting = [1] ++ (dropOnChance [2 .. n] randReals pa)
# randReals = drop n randReals
# initial = [1] ++ (dropOnChance [2 .. n] randReals pi)
= { size = [1 .. n],
	delta = randDeltas,
	accept = accepting,
	initial = initial}
\end{lstlisting}

Where \texttt{n} is the amount of states in the final automaton, \texttt{v} is the amount of
letters in the alphabet, \texttt{r} is the transition density, \texttt{pa} is the probability of a
state being an accepting state, \texttt{pi} is the probability of a state being an initial state
and \texttt{randReals} is an infinite list of random Reals between 0 and 1 that have been read from
\texttt{/dev/urandom}.

\subsubsection{Tranition density}
In order to calculate the probability that a transition should not be removed from the list of all
possible transitions, a translation must be made from the transition density to the aforementioned
probability. This is simply done using the expected value.

Assuming that every transition has an equal probability, we want the expected value to be: $r \cdot
n \cdot v$. If we then use the formula for the expected value, we get: $$r \cdot n \cdot v = n
\cdot n \cdot v \cdot p$$ where $p$ is the probability that we want. Rewriting the formula gives:
$$p = \frac{r}{n}$$ As such, that is how the probability is calculated on line 3.

This code is used to generate 1820 automata, 20 for every number of states between 10 and 100.

\section{Integrity}
In order to ensure the integrity of the benchmark, several measures were implemented. I will go
over all factors that I considered could have an influence on the integrity, and
explain how I tried to avoid them.

\paragraph{Caching}
Caching is a hardware implemented behaviour by the CPU that allows some part of RAM to be stored in
small temporary storage closer to the CPU. This enables faster access to parts of code and
constants that are used often. It is possible that either Hopcroft's algorithm or Brzozowski's
algorithm benefits more from this than the other. As such, the program, and all it's data
structures, are removed from memory upon completion of the minimization of an automaton. Note that
this does not completely disable caching. Rather, it reduces that advantage and either algorithm
might have over the other.

\paragraph{Parsing}
Two I/O operations on the same data in a computer can take a different time. I think this
difference is enough to influence the result. As such, the reading (and parsing) of the automata is
not part of the time that is counted for a run of either of the algorithms.

\paragraph{Auto Clocking/Turbo Boost}
The motherboard and CPU used in the benchmarking machine support auto clocking and turbo boost.
These technologies are designed to reduce the power use and temporarily increase the power of a PC
respectively. If the clock speed is reduced or increased during certain parts of the benchmark,
this could be in favor of either of the algorithms. Therefore, both Auto Clocking and Turbo Boost
were disabled for the benchmark.

\paragraph{Others}
In order to further improve the integrity, the benchmark was run on a single day on a single
machine. The machine was not used during the run of the benchmark, and all non-essential processes
were killed before the benchmark was run.

\paragraph{Specs}
The specs of the machine were:
\begin{description}
	\item[OS] Arch Linux x64
	\item[CPU] Intel Core i5-4460 @ 3.20GHz
	\item[RAM] 8GB @ 1333 MHz
	\item[Auto Clocking/Turbo Boost] Disabled
\end{description}
