\chapter{Introduction}\label{introduction}
Automata theory is a branch of computing science that studies the theory and applications of
automata. Automata are basic abstract models of computation that recognize some language. A basic
problem in automata theory is finding the minimized form of an automaton. A minimized automaton is
an automaton with the same language and a minimal number of states.

Automata minimization is used in many practical applications, and as such, it is desired that the
complexity of running these automata is kept at a minimum. There are several algorithms that are
used for minimization.  The best known (and understood) algorithms are arguably
Hopcroft's~\cite{hopcroft71}, Brzozowski's~\cite{brzozowski62} and Moore's~\cite{moore56}
algorithms. The theoretical performance of these algorithms is well understood, but not much is
known about their practical performance~\cite{almeida2007}. It should be noted that the practical
performance of automata minimizing algorithms need not be the same as the average theoretical
performance, as the automata that are used in practice might not be equally distributed among all
possible automata. Almeida et al.~\cite{almeida2007}, Watson~\cite{watson95}, and Tabakov and
Vardi~\cite{tabakov2005experimental} tried to find out which algorithm performs best in practice. All
these attempts use randomly generated automata.  Randomly generated automata might not accurately
represent automata that are used in real world applications, since there might be some
characteristics of automata that are more frequent than others in the real world. These
characteristics would not be accounted for by a random generator.

The results of Watson show that Brzozowski's algorithm performs better than Hopcroft's algorithm,
which is unexpected when considering the exponential versus logarithmic worst case complexities.
This thesis sets out to support or disprove that result by using a benchmark set instead of randomly
generated automata.

The benchmark set used in this thesis was obtained from the ``intermediate steps of model
checking''~\cite{abdulla2010simulation} with the purpose of being used in a benchmark. While these
automata might not accurately represent all automata that are used in practice, they could still
provide insight into possible differences between the performance of the algorithms when used on
random automata, versus practical automata. In order to provide a fair comparison, the benchmark
set will be compared to randomly generated automata. The random automata are generated using a
method described by Tabakov and Vardi~\cite{tabakov2005experimental} and both the benchmark set and
the randomly generated automata will be tested on the same implementations.

As described in Chapter 6, the results of my research show that Hopcroft's algorithm outperforms
Brzozowski's algorithm in nearly all tested automaton. These results are in line with the results
obtained by Almeida et al.~\cite{almeida2007}, and Tabakov and
Vardi~\cite{tabakov2005experimental}.

This thesis is structured as follows. The second chapter contains the preliminaries and introduces
all notation used in the rest of the thesis. The third and fourth chapter introduce Brzozowski's
and Hopcroft's algorithms respectively. They also provide a basic overview and highlights of the
implementation. Chapter 5 describes how the benchmarks in this thesis were performed. Chapter 6
focuses on the experimental results that were obtained in similar research and provides a
comparison between their results and the ones acquired in this research.
