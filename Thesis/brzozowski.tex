\chapter{Brzozowski's Algorithm}
\section{Overview}
Brzozowski's algorithm is a conceptually easy algorithm that was first described by Brzozowski in
1962~\cite{brzozowski62} and has an exponential time and space worst case complexity. Pseudocode
of the algorithm can be found in Algorithm~\ref{alg:brzozowski}. Unlike Moore's and Hopcroft's
algorithms, Brzozowski's algorithm can also be applied to NFAs. Given the focus of this thesis,
however, the rest of the chapter will only consider DFAs.

\begin{algorithm}
	\caption{Brzozowski's Algorithm}
	\label{alg:brzozowski}
	\begin{algorithmic}[1]
		\REQUIRE{$DFA:=(\Sigma, S, s_0, \delta, F)$}
		\ENSURE{$DFA:=(\Sigma, S', s_0', \delta', F')$}

		\STATE{$det(rev(det(rev(DFA))))$}
	\end{algorithmic}
	Where ``det'' and ``rev'' are the determinization and reversal functions that were defined in the
	preliminaries.
\end{algorithm}

\section{Example}
In order to understand the workings of the algorithm, this section provides an example for the
algorithm. Consider the non-minimal automaton in Figure~\ref{fig:CA2}, Brzozowski's algorithm
starts by reversing the automaton. Figure~\ref{fig:BZ1} shows the reversal of this automaton.
\begin{figure}[H]
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
		node distance=2.8cm,semithick]

		\node[initial,state]   (0)              {$q_0$};
		\node[state]           (1) [below of=0] {$q_1$};
		\node[state]           (2) [right of=1] {$q_2$};
		\node[accepting,state] (3) [right of=2] {$q_3$};
		\node[accepting,state] (4) [above of=3] {$q_4$};

		\path
		(0) edge [bend right=30]  node {a}   (1)
		    edge                  node {b}   (2)
		(1) edge [bend right=30]  node {a}   (0)
		    edge                  node {b}   (2)
		(2) edge                  node {a}   (4)
		    edge                  node {b}   (3)
		(3) edge [loop below]     node {a,b} (3)
		(4) edge [loop above]     node {a,b} (4);

	\end{tikzpicture}
	\caption{Complete Automaton 2}
	\label{fig:CA2}
\end{figure}

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
		node distance=2.8cm,semithick]

		\node[accepting,state]     (0)              {$q_0$};
		\node[state]               (1) [below of=0] {$q_1$};
		\node[state]               (2) [right of=1] {$q_2$};
		\node[initial right,state] (3) [right of=2] {$q_3$};
		\node[initial right,state] (4) [above of=3] {$q_4$};

		\path
		(0) edge [bend left=30]  node {a}   (1)
		(1) edge [bend left=30]  node {a}   (0)
		(2) edge                  node {b}   (0)
		    edge                  node {b}   (1)
		(3) edge [loop below]     node {a,b} (3)
		    edge                  node {b}   (2)
		(4) edge [loop above]     node {a,b} (4)
		    edge                  node {a}   (2);

	\end{tikzpicture}
	\caption{Reversed Automaton 1}
	\label{fig:BZ1}
\end{figure}

Subsequently, the resulting NFA is converted to a DFA using the subset construction method
described in Algorithm~\ref{alg:subset}. The resulting DFA can be seen in Figure~\ref{fig:BZ2}
\begin{figure}[H] \centering \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto, node
	distance=2.8cm,semithick]

		\node[initial right,state] (34)                  {$q_3, q_4$};
		\node[state]               (234)   [left of=34]  {$q_2, q_3, q_4$};
		\node[accepting,state,align=center]     (01234) [left of=234] {$q_0, q_1, q_2$\\$q_3, q_4$};

		\path
		(34)   edge                node {a,b} (234)
		(234)  edge [loop above]   node {a}   (234)
               edge                node {b}   (01234)
		(01234) edge [loop above]  node {a,b} (01234);

	\end{tikzpicture}
	\caption{Determinized Automaton 1}
	\label{fig:BZ2}
\end{figure}

This procedure (reversal and determinization) is performed once more, to create
the automata in Figure~\ref{fig:BZ3}.
\begin{figure}[H]
	\centering
	\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
		node distance=3.5cm,semithick]

		\node[initial,state,text width=2cm,align=center]       (0)              {$\{\{q_0, q_1,$\\$q_2, q_3, q_4\}\}$};
		\node[state,text width=2cm,align=center]               (1) [right of=0] {$\{\{q_0,
		q_1,$\\$q_2, q_3, q_4\},$\\$\{q_2, q_3,	q_4\}\}$};
		\node[accepting,state,text width=2cm,align=center]     (2) [right of=1] {$\{\{q_0,
		q_1,$\\$q_2, q_3, q_4\},$\\$\{q_2, q_3, q_4\},$\\$\{q_3, q_4\}\}$};

		\path
		(0) edge [loop above] node {a}   (0)
			edge              node {b}   (1)
		(1) edge              node {a,b} (2)
		(2) edge [loop above] node {a,b} (2);

	\end{tikzpicture}
	\caption{Resulting DFA from Brzozowski's Algorithm}
	\label{fig:BZ3}
\end{figure}


\section{Implementation}\label{sec:brz_implementation}
The implementation of Brzozowski's algorithm consists of two main parts, reversal and subset
construction. I will consider both individually.

\subsubsection*{Reversal}
Unlike the pseudocode in Algorithm~\ref{alg:reversal}, the code does not consider the final states
seperately from the reversing of the edges. Rather, every transition is considered, and special
actions are performed if the originating DFA state is an initial state. After that, the new
transition is added to the NFA. Since DFAs and NFAs use a different data structure, NFA states are
constructed at runtime everytime a NFA state is encountered that does not exist yet.
\begin{lstlisting}
for (DFAState* d_state : states) {
	for (unsigned int i = 0; i < d_state->transitions.size(); i++) {
		(*@\makebox[\linewidth][l]{$\smash{\vdots}$}@*)
		n_state_from = result->states.at(d_state->id);
		(*@\makebox[\linewidth][l]{$\smash{\vdots}$}@*)
		n_state_to = result->states.at(d_state->transitions.at(i)->id);
		(*@\makebox[\linewidth][l]{$\smash{\vdots}$}@*)
		n_state_from->transitions.push_back(std::make_pair(n_state_to, i));
	}
}
\end{lstlisting}

\subsubsection*{Subset Construction}
The Implementation of subset construction is a translation of the pseudocode in
Algorithm~\ref{alg:subset}. There are, however, multiple ways to implement this algorithm. Subset
construction, when implemented with an optimal time complexity, has a space complexity of
$\Theta(2^n)$. Experimental results show that such an implementation is not desired in practice due
to the large nature of the automata. An alternate implementation exists that trades some time
efficiency for a $O(2^n)$ space complexity. This section first describes parts of the
implementation that are not linked to this difference, and will then compare the two versions.

In order to implement this algorithm with with a minimal time complexity, we must ensure that the
statements on Line 4 and 5 can be performed in minimal time.

If we consider Line 4 without any context, one might be tempted to say that this statement can be
performed in $O(1)$. However, since we must also collect all destinations in some accumulator, the
implementation will still be $O(|E|)$. See the Further Improvements section for a more detailed
reasoning.

In order to implement Line 4 with such a complexity, we need an $O(1)$ mapping from a DFA state to
the set of NFA states that it represents. In the code, I use a vector to achieve this:

\begin{lstlisting}
std::vector<std::vector<NFAState*>*>* d_to_n =
		new std::vector<std::vector<NFAState*>*>;
\end{lstlisting}

Whenever a new DFA is constructed during the algorithm, it is assigned an identifying integer. This
integer is also used as the index for the set of NFA states in the vector. One might argue that it
would have been a better idea to make the set of NFA states a property of the DFA state, but I
chose not to implement it in an attempt to keep the code more understandable.

Line 5 seems, again, implementable in $O(1)$. This assumption rests on the idea that, since we
already have all NFA states accumulated, we need but add the set to the \lstinline|d_to_n| vector
and the queue. We must, however, also consider that the DFA state might already exist, if we do not
there are cases in which the algorithm might not terminate. I have implemented two ways to achieve
this. The first is theoretically the fastest, but has a $\Theta(2^n)$ complexity. The other is a
little slower, but has a $O(2^n)$ complexity. I will no consider each of these ways.

\paragraph{Version 1 ($\Theta(2^n)$)}
Conceptually, we need to solve this issue by being able to access a DFA state given a set of NFA
states in $O(1)$. This seems impossible, since it would take at least $O(n)$ (with $n$ the number
of NFA states in the set) to determine uniqueness. We can, however, leech on the steps performed by
the accumulation of NFA states.

My specific implementation considers some set of NFA states as a bitvector $(s_n, s_{n-1}, \dots,
s_1, s_0)$ where $s_i$ is the NFA state with id $i$. This bitvector is, in turn, considered as a
integer and used as an index in a vector:

\begin{lstlisting}
std::vector<DFAState*>* n_to_d =
		new std::vector<DFAState*>(pow(2, states.size()));
\end{lstlisting}

The vector is created with size $2^{|S|}$ in order to fix the maximum integer that could result
from the bitvector.

Relevant parts of the code that mentioned above are:
\begin{lstlisting}
for (auto initial_state : initial_states) {
	index |= (ONE << initial_state->id);
}
\end{lstlisting}
Which is where the index is constructed to map a set of NFA states to a DFA state for the initial
states and:
\begin{lstlisting}
for (auto n_state_c : n_state->move(letter)) {
	if (!n_state_c->added_to_acc) {
		index |= (ONE << n_state_c->id);
		accumulator->push_back(n_state_c);(*@%
		\makebox[\linewidth][l]{$\smash{\vdots}$}@*)
	}
}
\end{lstlisting}
where the same is done for all other states. The actual lookup in the \lstinline|n_to_d| vector is
done in:
\begin{lstlisting}
DFAState* target = n_to_d->at(index);
if (target == nullptr) {
	target = new DFAState(DFA_id_counter, alphabet->size());
	DFA_id_counter++;
	d_to_n->push_back(accumulator);
	n_to_d->at(index) = target;(*@%
	\makebox[\linewidth][l]{$\smash{\vdots}$}@*)
}
\end{lstlisting}

\paragraph{Version 2 ($O(2^n)$)}
If we choose not to implement the step on Line 5 optimally (by using a \lstinline|std::map|), we
can reduce the space complexity to $O(2^n)$ at the cost of some performance. The idea is relatively
simple; we replace the \lstinline|n_to_d| vector with a map. This allows us not to have to
initialize the data structure, which in turn means that we do not have to reserve a $2^n$ size
memory block. Version 2 is the version that is used for the benchmarks in this thesis.

\section{Further Improvements}
My implementation of Brzozowski's algorithm can still be improved on some fronts to increase the
efficiency. In this section I will consider some of the improvements that I considered
implementing, but were left out due to time constraints.

\subsubsection*{Reversal}
In order to provide context to the changes I suggest, important parts of the code are provided
first.
\begin{lstlisting}
NFAState* n_state_from;
NFAState* n_state_to;
for (DFAState* d_state : states) {
	for (unsigned int i = 0; i < d_state->transitions.size(); i++) {
		if (result->states.at(d_state->id) == nullptr) {
			n_state_from = new NFAState(d_state->id);
			result->states.at(d_state->id) = n_state_from;
		} else {
			n_state_from = result->states.at(d_state->id);
		}
		if (result->states.at(d_state->transitions[i]->id) == nullptr) {
			n_state_to = new NFAState(d_state->transitions[i]->id);
			result->states.at(d_state->transitions[i]->id) = n_state_to;
		} else {
			n_state_to = result->states.at(d_state->transitions[i]->id);
		}
		n_state_to->transitions.push_back(std::make_pair(n_state_from, i));
	}
}
\end{lstlisting}
Currently, there are several parts of the code that need not be in the inner loop. Since we go
through every transition of a given state, we know that \lstinline|n_state_to| will stay the same
in a single iteration of the outer for-loop. This means that we could move the check for a nullptr,
and the initial/accepting state check one for-loop up. We can do even better by considering the
accepting state outside the outer for-loop. This can easily be done by using the fact that
the DFA data structure contains a vector with pointers to the accepting states.

\subsubsection*{Subset Construction}
Currently, vectors are used to store the NFA states to DFA state mapping. Since the size is known
during the initialization of the vector, using an array could significantly reduce the overhead.

In Section~\ref{sec:brz_implementation} I discussed the different data structures that can be used
to store the \lstinline|n_to_d| mapping. There is another option that lays outside the scope of
this thesis, and has the same complexity as a map. Despite the same complexity as the map, in
practice, it might perform differently from \lstinline|std::map|. The concept of the new data
structure is that it is a tree that is traversed as the transitions are considered. In theory,
this means that the tree has been fully traversed after the inner forloop. One difficulty with this
implementation is that every transition must be considered concurrently and in order. Forcing the
incremental order of the transitions can be done in construction. If we then use a merging
algorithm to merge all transition vectors, we can get a single vector containing all destination
states. Merging two vectors of length $n$ and $m$ van be done in $O(m+n)$. As such, access and
insertion can be done in the same complexity as in a \lstinline|std::map|.
