\chapter{Related Work/Discussion}\label{sec:relatedwork}
There has been some research into the practical performance of automata minimization algorithms,
most notably the work by Watson~\cite{watson95}, Tabakov and Vardi~\cite{tabakov2005experimental},
and Almeida et al~\cite{almeida2007}. I will consider each of these works in chronological order. For each of the works I will explain the reasoning behind their research as well as the results that they obtained, followed by a discussion regarding the results compared to those obtained in this thesis.

\section{Watson, 1995}
Watson indicated that is is hard to compare algorithms that achieve the same result because of
several factors.
\begin{enumerate}
	\item Different programming languages
	\item No implementations
	\item No collective framework
\end{enumerate}
All of these boil down to a single, comprehensive problem, there is no set of implemented
algorithms that are part of the same framework. He argues that this makes it impossible to
correctly compare the performance of these algorithms. As such, in his thesis, Watson provides such
a framework and compares the running time of the algorithms he implemented.

The result of his experiments can be found in Figure~\ref{watsonresults}.

\begin{figure}
	\includegraphics[width=\textwidth]{watsonresults.png}
	\caption{Median performance versus DFA states by Watson (lower is better)}
	\label{watsonresults}
\end{figure}

BRZ is Brzozowski's algorithm, HOP is Hopcroft's algorithm, and BW is an algorithm due to Watson
himself. Noteworthy is the fact that Brzozowski's algorithm perform consistently better than
Hopcroft's algorithm, and that Watson's algorithm performs better than Brzozowski's algorithm in
most cases. This is exceptionally unintuitive when considering their theoretical complexity.

The DFAs that Watson used in his research were obtained from random regular expressions or they were
generated from ``some other specification''~\cite{watson95}.

When comparing these results to the ones obtained in this thesis, Hopcroft's algorithm appears to perform faster than Brozowski's algorithm, instead of the other way around.
This can be clearly seen in Figure~\ref{fig:res_bench} and \ref{fig:res_rand}.

\section{Tabakov and Vardi, 2005}
Tabakov and Vardi tested the algorithms on randomly generated automata. In particular, they devote a
relatively large section of their paper to determining the best way to generate such automata. It
is this study that I use to generate the random automata in the thesis.

Their results show that Brzozowski performs significantly better when the automata to minimize has
a high ($> 1.5$) transition density.

While this doesn't go completely against the results that were found by Watson, it does show that
Brzozowski's algorithm does not perform strictly better than Hopcroft's algorithm.

When comparing Figure~\ref{fig:dens_plot} with those provided by Tabakov and
Vardi~\cite{tabakov2005experimental}, it is clear that the benchmark set does not have a wide
enough range of transition densities to draw a significant conclusion. One can see, however, that
the ratio increases as the transition density increases. Unfortunately, this ratio cannot be
compared to the data as presented by Tabakov and Vardi, because they do not provide good
contrast in the lower ranges of the transition density.

\input{bench_plot_2}
With regards to the number of states, Tabakov and Vardi noted that ``At the peak,
Hopcroft’s algorithm scales exponentially, but generally the algorithms scale
subexponentially''~\cite{tabakov2005experimental}. When we make slight modifications to the
representation of the results in Figure~\ref{fig:res_bench}, similar conclusions can be drawn from
my experiments (see Figure~\ref{fig:res_bench_2}).

\input{rand_plot_2}
Comparing the results that I obtained (see Figure~\ref{fig:res_rand_2}) on the random automata,
with the results obtained by Tabakov and Vardi for a density of 1.25, shows similar growth as well.

\section{Almeida et al, 2007}
Almeida et al.\ argue that the automata used by Watson are biased, and suggest using random automata
to assess the performance of the algorithms. Their approach to generating automata is
different from the one used by both Watson (using regular expressions), and Tabakov and Vardi
(using their own generator). Instead, they opt to represent automata as a string, and then randomly
generating these strings. They also argue, however, that this method provides uniform random
samples, and thus does not represent typical use of the algorithms.

Their results show that Brzozowski's algorithm (as well as Watson's algorithm) does not perform well
in any case, and that an improved version of Watson's algorithm (with memoization) performs better
than Hopcroft's algorithm for automata with more than 10 states. In their experiments on pseudo
randomly generated NFAs, they show that Hopcroft's algorithm and Brzozowski's algorithm perform
nearly identical for a variety of transition densities. Unfortunately, a comparison with the results
obtained by Tabakov and Vardi is hard, due to the small range of transition densities that were
tested by Almeida et al.

Almeida et al.\ show that Brzozowski's algorithm is largely outperformed by Hopcroft's algorithm
when used on the equally distributed DFAs~\cite{almeida2007}. While this is more in line with the
results obtained in this thesis, the time limit that was opposed by Almeida et al.\ leads to
incomplete data on the performance of Brzozowski's algorithm. Therefore, we can only compare the
results that were obtained for automata with 10 states. For these automata, the results of the two
experiments seem very similar, with Hopcroft's algorithm performing approximately a factor 10
better than Brzozowski's algorithm.

Almeida et al.\ also provide results of experiments where transition densities were compared.
Their experiments include sets with a transition density of 0.2, 0.5 and 0.8, with an alphabet size
of 2 and 5 letters. The results that they obtained show virtually identical performance between the
two algorithms, even when subset construction is included in the timing of the algorithms. As
mentioned before, it is hard to compare these results to those of Tabakov and Vardi. It is,
however, interesting to note that these results do not resemble the results that were obtained in
this thesis. This discrepancy is likely due to the fact that this thesis didn't use randomly
generated automata to get the data.

\section{Conclusion}
In conclusion, this thesis shows that Hopcroft's algorithm outperforms Brzozowski's algorithm in
the benchmark set. However, due to the relatively small transition densities of the automata in the
benchmark set, a strict conclusion cannot be drawn in the general case. If the assumption is made
that the benchmark set fairly represents the automata used in practice, Hopcroft's algorithm is
shown to perform better in practice.

\section{Further Work}
The main issue with the benchmark set used in this thesis is that it might not accurately represent
the automata that are used in practice. In order to ensure that this is the case, it might be a
sensible idea to collect automata that are actually used in practice. I propose that regular
expressions are collected from source code files in public repositories (e.g. GitHub) and are
subsequently converted to automata to form a new benchmark set.

Although not for minimization, the fact that these automata are from source code repositories
ensures that they are at least used in practice.
