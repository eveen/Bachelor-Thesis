\documentclass{scrartcl}

\usepackage[style=ieee,backend=biber]{biblatex}
\bibliography{../Thesis/bibtex}

\title{Research Plan\\
	The Practical Performance of Automata Minimizing Algorithms}
\author{Erin van der Veen\\
	Supervised by dr J.C. Rot}

\begin{document}

\maketitle

\section*{Problem Description} Automata minimization is used in many practical
applications, and as such, it is desired that the complexity of running these
automata is kept at a minimum.

There are several algorithms that are used to achieve the task of minimization.
The best know (and understood) algorithms are arguably Hopcroft, Brzozowski and
Moore. Another algorithm was introduced by Watson in 1995.

Moore's algorithms uses a technique called `partition refinement` in a naive
way to perform minimization.  Hopcroft uses a very similar approach, in a less
naive way. As such, I will not include Moore's algorithm in the paper.

The theoretical performance of these algorithms is well understood, but not
much is known about their practical performance~\cite{almeida2007}.

Please note that the practical performance of Automata minimizing algorithms
need not be the same as the average theoretical performance, as the automata
that are used in practice might not be equally distributed among all possible
automata.

Both Alemeida\cite{almeida2007} et al. and Watson\cite{watson95} try to answer
the same question. Unfortunately both attempts use randomly generated automata.
Randomly generated automata might not accurately represent automata that are
used in real world applications, since there might be some characteristics of
automata that are more frequent then others in the real world. These
characteristics would not be accounted for by a random generator. In this
project, I will attempt to use automata that more accurately represent
real-world automata.

\section*{Research Question}
What is the performance of Hopcroft's, Brzozowski's and Watson's
alrogithm when used in practical applications?

\section*{Expected Results}
The theoretical complexity of the algorithms should give some indication as to
what their practical performance might be.  The worst case complexity of the
algorithms are as follows:

\begin{description}
	\item[Hopcroft] $\mathcal{O}(kn \log n)$~\cite{hopcroft71} with k the
		size of the alphabet and n the number of states
	\item[Brzozowski] Exponential~\cite{almeida2007}
	\item[Watson] $\mathcal{O}(n^2\alpha(n^2))$ where $\alpha$ is the
		inverse of the Ackerman function~\cite{almeida2007}
\end{description}

As such, I expect Hopcroft's algorithm to perform best in the benchmarks,
followed by Watson's algorithm, with Brzozowski's algorithm last.

This is the same order that was obtained by Almeida et al. in their research.

\section*{Strategy}
In order to answer this question, I will first have to figure out how to solve
two other problems:

\begin{enumerate}
	\item How should the aforementioned algorithms be implemented
		in order to achieve the optimal complexity.
	\item What DFAs most accurately represent DFAs that are used
		in practical applications, and how can these DFAs
		best be obtained?
\end{enumerate}

After thoroughly understanding every algorithm, I will focus my attention to
implementing every algorithm in C(++). Since I, like I mentioned before, I want
to test the algorithms on automata that are not randomly generated, I will use
a collection of automata that is intended to be used for benchmarking
(\footnote{https://github.com/lorisdanto/automatark}). Since the idea with benchmarking is
that they represent real-world scenarios, the automata from this repository are
more suitable for measuring the practical performance. This is, of course, an
assumption that must be studied further.

The benchmark collection only contains NFA's, so I will have to first
convert the NFA's to DFA's.

All my implementations, and perhaps those used by Almeida et al. will be timed
on all automata. The results will be analyzed on several aspects. If possible,
I will also log the amount RAM needed by the algorithms to perform each of the
minimizations. If time allows it, looking at the difference between running
Brzozowski on an NFA, and running Brzozowski on the DFA that was obtained by
determinizing the NFA, might be interesting to include in the project.

It is also optionally possible to use the automata generator used by Almeida et
al. in order to compare the implementations and make it easier to compare my
results with theirs.

\printbibliography
	
\end{document}
