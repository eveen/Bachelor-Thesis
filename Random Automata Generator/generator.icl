module generator

import StdEnv
from StdListExtensions import foldlSt

:: NFA = {
			size :: [Int],
			delta :: [(Int, Int, Int)],
			accept :: [Int],
			initial :: [Int]
		}

:: Temp :== [(Int, Int, Int)]

Start world
# (randomlist, world) = random_reals world
= nfaToTimbuk (random_nfa 10 10 0.5 0.5 0.5 randomlist)

//Start world
//# (hoi, world) = random_reals world
//= take 5 hoi

random_reals :: !*World -> ([Real], !*World)
random_reals world
# (_, urandom, world) = fopen "/dev/urandom" FReadData world
# (list, _) = random_reals` urandom
= (map (\x -> abs ((toReal (x rem 1000000000)) / (toReal 1000000000))) list, world)

random_reals` :: *File -> ([Int], *File)
random_reals` urandom
# (_, rndInt, urandom) = freadi urandom
# (list, urandom) = random_reals` urandom 
= ([rndInt : list], urandom)

// Nodes, Variables, Transitions Density, Probability Accepting
random_nfa :: Int Int Real Real Real [Real] -> NFA
random_nfa n v r pa pi randReals
# d = r / (toReal n)
# randDeltas = diag3 [1 .. n] [1 .. v] [1 .. n]
# randDeltas = dropOnChance randDeltas randReals d
# randReals = drop (n * n * v) randReals
# accepting = dropOnChance [1 .. n] randReals pa
# randReals = drop n randReals
# initial = dropOnChance [1 .. n] randReals pi
= { size = [1 .. n],
	delta = randDeltas,
	accept = accepting,
	initial = initial}

dropOnChance :: [a] [Real] Real -> [a]
dropOnChance [] _ _ = []
dropOnChance [x : xs] [c : cs] d
| c <= d = [x : dropOnChance xs cs d]
| otherwise = dropOnChance xs cs d

nfaToTimbuk :: NFA -> String
nfaToTimbuk nfa
# res = "Automaton a\n" 
# res = res +++ "States " +++ printStates nfa.size +++ "\n"
# res = res +++ "Final States " +++ printStates nfa.accept +++ "\n"
# res = res +++ "Transitions" +++ printInitial nfa.initial
# res = res +++ printDeltas nfa.delta
= res

printStates :: [Int] -> String
printStates [] = ""
printStates [x : xs] = "q" +++ (toString x) +++ " " +++ printStates xs

printDeltas :: [(Int, Int, Int)] -> String
printDeltas [] = ""
printDeltas [(rom, letter, to) : ds] = "a" +++ (toString letter) +++ "(q" +++ (toString rom)
	+++ ") -> q" +++ (toString to) +++ "\n" +++ printDeltas ds

printInitial :: [Int] -> String
printInitial [] = ""
printInitial [i : is] = "x -> q" +++ (toString i) +++ "\n" +++ printInitial is
