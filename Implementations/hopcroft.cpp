#include "hopcroft.hpp"

void Hopcroft::minimize() {
	// Preparational work
	calc_inverse_table();

	// Line 1
	create_accepting_partition();

	if (PI[1]->states.empty())
		PI.pop_back();

	push_splitter(PI.back());

	Splitter* s;

	// Line 7,8
	while (!W.empty()) {
		s = W.back(); W.pop_back();
		s->p->splitters.pop_back();

		// Line 9-19
		for (Partition* p : splitted_by(s)) {
			replace_partition(p);
		}
	}
}

// Doesn't actually calculate a table. Instead just flips the transitions in a DFAState.
// The reversal is needed to find all partitions that are split given some splitter
void Hopcroft::calc_inverse_table() {
	for (DFAState* state : dfa->states) {
		state->reverse_transitions.resize(state->transitions.size());
	}

	for (DFAState* state : dfa->states) {
		for (unsigned int i = 0; i < state->transitions.size(); i++) {
			state->transitions[i]->reverse_transitions[i].push_back(state);
		}
	}
}

void Hopcroft::create_accepting_partition() {
	Partition* accepting = new Partition;
	Partition* non_accepting = new Partition;

	for (DFAState* state : dfa->states) {
		if (state->accepting) {
			accepting->states.push_back(state);
			state->p = accepting;
		} else {
			non_accepting->states.push_back(state);
			state->p = non_accepting;
		}
	}

	if (accepting->states.size() < non_accepting->states.size()) {
		PI.push_back(non_accepting);
		PI.push_back(accepting);
	} else {
		PI.push_back(accepting);
		PI.push_back(non_accepting);
	}
}

void Hopcroft::push_splitter(Partition* p) {
	Splitter* temp;
	for (unsigned int i = 0; i < dfa->states[0]->transitions.size(); i++) {
		temp = new Splitter;
		temp->p = p;
		temp->letter = i;
		p->splitters.push_back(temp);
		W.push_back(temp);
	}
}

std::list<Partition*> Hopcroft::splitted_by(Splitter* splitter) {
	std::list<Partition*> result;

	for (DFAState* s : splitter->p->states) {
		for (DFAState* temp_state : s->reverse_transitions[splitter->letter]) {
			if (temp_state && !(temp_state->different_part)) {
				temp_state->different_part = true;
				Partition* temp = temp_state->p;
				if (temp->times_added == 0) {
					result.push_back(temp);
				}
				temp->times_added++;
			}
		}
	}

	std::list<Partition*>::iterator i = result.begin();
	while (i != result.end()) {
		if ((*i)->times_added == (*i)->states.size()) {
			for (DFAState* state : (*i)->states)
				state->different_part = false;
			(*i)->times_added = 0;
			result.erase(i++);
		} else {
			++i;
		}
	}

	for (Partition* part : result)
		part->times_added = 0;

	return result;
}

void Hopcroft::replace_partition(Partition* p) {
	Partition* p_ = new Partition;

	for (DFAState* state : p->states) {
		if (state->different_part) {
			state->p = p_;
			p_->states.push_back(state);
		}
	}

	auto is_in_new_part = [](DFAState* a) {return a->different_part;};
	p->states.remove_if(is_in_new_part);

	std::vector<bool> known_splitters(dfa->states[0]->transitions.size());

	for (Splitter* s : p->splitters) {
		known_splitters[s->letter] = true;
		replace_splitter(s, p_);
	}

	Partition* smallest = p_->states.size() < p->states.size() ? p_ : p;

	for (unsigned int i = 0; i < known_splitters.size(); i++) {
		if (!known_splitters[i]) {
			// Push smallest
			Splitter* temp = new Splitter;
			temp->p = smallest;
			temp->letter = i;
			smallest->splitters.push_back(temp);
			W.push_back(temp);
		}
		known_splitters[i] = false;
	}

	// Add new partition
	PI.push_back(p_);

	// Reset
	for (DFAState* state : p_->states) {
		state->different_part = false;
	}
}

void Hopcroft::replace_splitter(Splitter* s, Partition* new_part) {
	Splitter* temp;

	temp = new Splitter;
	temp->p = new_part;
	temp->letter = s->letter;
	new_part->splitters.push_back(temp);
	W.push_back(temp);
}

void Hopcroft::construct_dfa() {
	DFA* dfa = new DFA;
	unsigned int alphabet_size = this->dfa->states[0]->transitions.size();

	for (unsigned int i = 0; i < PI.size(); i++) {
		dfa->states.push_back(new DFAState(dfa->states.size(), alphabet_size));
	}

	unsigned int i = 0;
	for (Partition* part : PI) {
		part->id = i;
		i++;
	}

	for (Partition* part : PI) {
		if (std::any_of(part->states.begin(), part->states.end(),
				[](DFAState* a){return a->accepting;}))
			dfa->states[part->id]->accepting = true;
		if (std::any_of(part->states.begin(), part->states.end(),
				[](DFAState* a){return a->initial;}))
			dfa->states[part->id]->initial = true;
		for (unsigned int j = 0; j < alphabet_size; j++) {
			dfa->states[part->id]->transitions[j] =
					dfa->states[part->states.front()->transitions[j]->p->id];
		}
	}

	this->dfa = dfa;
}

void Hopcroft::print_partitions() {
	for (Partition* p : PI) {
		print_partition(p);
		std::cout << " | ";
	}
	std::cout << std::endl;
}

void Hopcroft::print_partition(Partition* p) {
	std::cout << "{ ";
	for (DFAState* s : p->states) {
		std::cout << s->id << ", ";
	}
	std::cout << "}";
}

void Hopcroft::print_splitter(Splitter* s) {
	std::cout << "( ";
	print_partition(s->p);
	std::cout << ", " << s->letter << " )";
}

void Hopcroft::print_splitters() {
	for (Splitter* s : W) {
		print_splitter(s);
		std::cout << " | ";
	}
	std::cout << std::endl;
}
