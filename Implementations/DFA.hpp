#ifndef DFA_H
#define DFA_H

#include <vector>
#include <iostream>
#include "NFA.hpp"

#include "hopcroft.hpp"

class NFA;
class Partition;

class DFAState {
	public:
		DFAState(unsigned int id, unsigned int alphabet_size);

		DFAState move(unsigned int letter);

		std::vector<DFAState*> transitions;

		// Used for O(1) in vectors
		unsigned int id;
		bool initial = false;
		bool accepting = false;

		// Used exclusively in Hopcroft
		Partition* p;
		std::vector<std::list<DFAState*>> reverse_transitions;
		bool different_part = false;
};

class DFA {
	public:
		std::vector<DFAState*> states;

		// No need to store the same States twice
		DFAState* initial_state;
		std::vector<DFAState*> final_states;

		NFA* reverse();
		
		// Used for debugging
		friend std::ostream& operator<<(std::ostream& os, const DFA& dfa);

	private:

};
#endif
