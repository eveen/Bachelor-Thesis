#ifndef NFA_H
#define NFA_H

#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <string>
#include <regex>
#include <queue>
#include <cmath>
#include "DFA.hpp"

class DFA;

class NFAState {
	public:
		NFAState(unsigned int id);

		std::vector<NFAState*> move(unsigned int letter);
		std::vector<std::pair<NFAState*, unsigned int>> transitions;

		// Used for O(1) access to vectors
		unsigned int id;
		bool initial = false;
		bool accepting = false;

		bool added_to_acc = false;
};

class NFA {
	public:
		NFA(char* filename);
		NFA(unsigned int nr_of_states, unsigned int alphabet_size);

		//TODO: Destructor
		
		std::vector<NFAState*> states;

		// Pointer to allow storing on heap
		std::vector<unsigned int>* alphabet;

		// To allow O(1) access to initial and final states
		// prevents having to go through all states
		std::vector<NFAState*> initial_states;
		std::vector<NFAState*> final_states;

		DFA* determinize_speed();
		DFA* determinize_memory();

		// Used for debugging
		friend std::ostream& operator<<(std::ostream& os, const NFA& nfa);

	private:
		unsigned int state_id = 0;
		// Alphabet counter
		unsigned int alphabet_counter = 0;
		void create_alphabet(std::map<unsigned int, unsigned int>*);
		bool starts_with(std::string*, std::string*);
		void parse_states(std::string*, std::map<unsigned int, NFAState*>*);
		void parse_final_states(std::string*, std::map<unsigned int, NFAState*>*);
		void parse_initial_state(std::string*, std::map<unsigned int, NFAState*>*);
		void parse_transition(std::string*, std::map<unsigned int, NFAState*>*, std::map<unsigned int, unsigned int>*);
};

#endif
