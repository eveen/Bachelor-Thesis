#ifndef HOPCROFT_H
#define HOPCROFT_H

#include <list>

#include "DFA.hpp"
#include "NFA.hpp"

class DFA;
class DFAState;
class Splitter;

class Partition {
	public:
		std::list<DFAState*> states;
		unsigned int times_added = 0;

		// Used to index partition vector PI in O(1)
		// Usefull when replacing a partition
		// We do not care if partitions are in order
		unsigned int id;
		std::list<Splitter*> splitters;
};

class Splitter {
	public:
		Partition* p;
		unsigned int letter;
		unsigned int pos_in_queue;
};

class Hopcroft {
	public:
		void minimize();
		DFA* dfa;
		void construct_dfa();

	private:
		void calc_inverse_table();
		void create_accepting_partition();
		void push_splitter(Partition*);
		std::list<Partition*> splitted_by(Splitter*);
		void replace_partition(Partition*);
		void replace_splitter(Splitter*, Partition*);

		std::vector<Partition*> PI;
		std::list<Splitter*> W;

		void print_partitions();
		void print_partition(Partition*);
		void print_splitter(Splitter*);
		void print_splitters();
};

#endif
