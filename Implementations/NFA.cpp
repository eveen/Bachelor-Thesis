#include "NFA.hpp"

NFAState::NFAState(unsigned int id) {
	this->id = id;
}

std::vector<NFAState*> NFAState::move(unsigned int letter) {
	std::vector<NFAState*> result;
	for (auto transition : transitions) {
		if (transition.first != nullptr && transition.second == letter)
			result.push_back(transition.first);
	}
	return result;
}

NFA::NFA(char* filename) {
	// Local buffer to store read line before parsing
	std::string line;

	//       letter        my representation
	std::map<unsigned int, unsigned int> alphabet_map;

	// Initialize alphabet
	alphabet = new std::vector<unsigned int>();

	// Used to index the states based on the id of the file
	// The states are also given a unique id from 0 to n
	std::map<unsigned int, NFAState*> states_map;

	// Used to find certain sections in the file
	std::string prefix;

	// Open the given file
	std::ifstream nfa_file(filename);

	if(nfa_file.is_open()) {
		// Read untill the States line
		prefix = "States";
		do {
			std::getline(nfa_file, line);
		} while(!starts_with(&line, &prefix));
		// Parse states
		parse_states(&line, &states_map);

		// Parse final_states
		std::getline(nfa_file, line);
		parse_final_states(&line, &states_map);

		// Parse initial states
		prefix = "x ->";
		do {
			std::getline(nfa_file, line);
		} while(!starts_with(&line, &prefix));
		
		while(starts_with(&line, &prefix)) {
			parse_initial_state(&line, &states_map);
			std::getline(nfa_file, line);
		}

		// Parse trasitions
		do {
			parse_transition(&line, &states_map, &alphabet_map);
		} while(std::getline(nfa_file, line));

		nfa_file.close();

		create_alphabet(&alphabet_map);
	}
}

NFA::NFA(unsigned int nr_of_states, unsigned int alphabet_size):states(nr_of_states) {
	alphabet = new std::vector<unsigned int>();
	for (unsigned int i = 0; i < alphabet_size; i++)
		alphabet->push_back(i);
	initial_states = std::vector<NFAState*>();
	final_states = std::vector<NFAState*>();
}

void NFA::create_alphabet(std::map<unsigned int, unsigned int>* am) {
	for (auto e : *am) {
		alphabet->push_back(e.second);
	}
}

bool NFA::starts_with(std::string* s, std::string* prefix) {
	return s->compare(0, prefix->length(), *prefix) == 0;
}

void NFA::parse_states(std::string* line, std::map<unsigned int, NFAState*>* v) {
	std::smatch m;
	std::regex e("q(\\d+)");

	while(std::regex_search(*line, m, e)) {
		NFAState* temp = new NFAState(state_id);
		v->insert(std::make_pair(stoi(m.str(1)), temp));
		states.push_back(temp);
		*line = m.suffix().str();
		state_id++;
	}
}

void NFA::parse_final_states(std::string* line, std::map<unsigned int, NFAState*>* v) {	
	std::smatch m;
	std::regex e("q(\\d+)");

	while(std::regex_search(*line, m, e)) {
		auto it = v->find(stoi(m.str(1)));
		if (it != v->end()) {
			it->second->accepting = true;
			final_states.push_back(it->second);
		}
		*line = m.suffix().str();
	}
}

void NFA::parse_initial_state(std::string* line, std::map<unsigned int, NFAState*>* v) {
	std::smatch m;
	std::regex e("q(\\d+)");

	std::regex_search(*line, m, e);
	auto it = v->find(stoi(m.str(1)));
	if (it != v->end()) {
		it->second->initial = true;
		initial_states.push_back(it->second);
	}
}

void NFA::parse_transition(std::string* line, std::map<unsigned int, NFAState*>* v, std::map<unsigned int, unsigned int>* am) {
	std::smatch m;
	std::regex e("a(\\d+)\\(q(\\d+)\\).*q(\\d+)");

	std::regex_search(*line, m, e);
	auto it_s = v->find(stoi(m.str(2)));
	auto it_d = v->find(stoi(m.str(3)));
	unsigned int letter;
	if (it_s != v->end() && it_d != v->end()) {
		auto it_a = am->find(stoi(m.str(1)));
		if (it_a != am->end()) {
			letter = it_a->second;
		} else {
			letter = alphabet_counter;
			am->insert(std::make_pair(stoi(m.str(1)), alphabet_counter));
			alphabet_counter++;
		}
		// Possible improvements:
		// Counter number of letter first,
		// then use a vector with sets
		// to allow O(1) access of edges
		// DOES NOT reduce complexity
		// because every edge must be considered anyway
		it_s->second->transitions.push_back(std::make_pair(it_d->second, letter));
	}

}

// Determinize by sub construction
// Optimized for speed
DFA* NFA::determinize_speed() {
	DFA* result = new DFA();

	// Used to associate single DFA state with a set of NFAStates
	// Since we can guarantee that the DFA states are created in order 0 -> n
	// the vector need not be instantiated with a size
	std::vector<std::vector<NFAState*>*>* d_to_n =
			new std::vector<std::vector<NFAState*>*>;

	// Used to associate a set of NFA states with a DFA state
	// the set of NFA's is represented as a bitstring, with the
	// last bit representing state 0
	// This bit representation is used as an index to get the DFA state
	// This is done to enable O(1) association from NFA to DFA states
	std::vector<DFAState*>* n_to_d =
			new std::vector<DFAState*>(pow(2, states.size()));

	// Contains DFA states that must be considered
	std::queue<DFAState*> work;

	// Used in combination with bitshifts to obtain the index in n_to_d
	// given a set of NFA states
	unsigned long long index = 0;
	// In order to allocate enough space, we use a unsigned long long
	const unsigned long long ONE = 1;

	unsigned int DFA_id_counter = 0;

	// Push initial state
	work.push(new DFAState(DFA_id_counter, alphabet->size()));
	DFA_id_counter++;
	work.front()->initial = true;
	result->initial_state = work.front();
	result->states.push_back(work.front());

	// Initial state must accept if the NFA also accepts
	// on the initial state
	for (auto final_state : initial_states) {
		if (final_state->accepting) {
			work.front()->accepting = true;
			result->final_states.push_back(work.front());
			break;
		}
	}

	for (auto initial_state : initial_states) {
		index |= (ONE << initial_state->id);
	}

	// Setup references
	d_to_n->push_back(&initial_states);
	n_to_d->at(index) = work.front();

	// Used to create the d_to_n vector;
	std::vector<NFAState*>* accumulator;

	while (!work.empty()) {
		// Get DFAState to be considered
		DFAState* d_state = work.front();
		work.pop();

		for (auto letter : *alphabet) {
			index = 0;
			// Kept to pass to the d_to_n vector
			accumulator = new std::vector<NFAState*>;
			
			// If any of the NFAStates is final, so is the DFAState
			bool is_final = false;
			for (auto n_state : *(d_to_n->at(d_state->id))) {
				for (auto n_state_c : n_state->move(letter)) {
					// add_to_acc allows O(1) prevention of duplicates
					// in the accumulator vector
					// TODO: Remove accumulator and use index and mask instead
					if (!n_state_c->added_to_acc) {
						index |= (ONE << n_state_c->id);
						accumulator->push_back(n_state_c);
						n_state_c->added_to_acc = true;
						is_final = n_state_c->accepting ? true : is_final;
					}
				}
				for (auto acc : *accumulator) {
					acc->added_to_acc = false;
				}
			}
			// Target is the newly collected set of NFAStates
			DFAState* target = n_to_d->at(index);

			// Doesn't exist yet
			if (target == nullptr) {
				target = new DFAState(DFA_id_counter, alphabet->size());
				DFA_id_counter++;
				d_to_n->push_back(accumulator);
				n_to_d->at(index) = target;
				// If it already exists, it has to already
				// be in the queue, or at least have been
				// in the queue
				work.push(target);

				// Update result
				if (is_final) {
					result->final_states.push_back(target);
					target->accepting = true;
				}
				result->states.push_back(target);
			} else {
				// Clean up some memory
				delete accumulator;
			}
			
			// Add transition
			d_state->transitions.at(letter) = target;
		}
	}
	// Finally return result
	return result;
}

// Determinize by sub construction
// Trades of some speed for a much better memory footprint
DFA* NFA::determinize_memory() {
	DFA* result = new DFA();

	// Used to associate single DFA state with a set of NFAStates
	// Since we can guarantee that the DFA states are created in order 0 -> n
	// the vector need not be instantiated with a size
	std::vector<std::vector<NFAState*>*>* d_to_n =
			new std::vector<std::vector<NFAState*>*>;

	// Used to associate a set of NFA states with a DFA state
	// the set of NFA's is represented as a bitstring, with the
	// last bit representing state 0
	// This bit representation is used as an index to get the DFA state
	// This is done to enable O(1) association from NFA to DFA states
	std::map<std::vector<bool>, DFAState*> n_to_d;

	// Contains DFA states that must be considered
	std::queue<DFAState*> work;

	// Used in combination with bitshifts to obtain the index in n_to_d
	// given a set of NFA states
	std::vector<bool> index(states.size());

	unsigned int DFA_id_counter = 0;

	// Push initial state
	work.push(new DFAState(DFA_id_counter, alphabet->size()));
	DFA_id_counter++;
	work.front()->initial = true;
	result->initial_state = work.front();
	result->states.push_back(work.front());

	// Initial state must accept if the NFA also accepts
	// on the initial state
	for (auto final_state : initial_states) {
		if (final_state->accepting) {
			work.front()->accepting = true;
			result->final_states.push_back(work.front());
			break;
		}
	}

	for (auto initial_state : initial_states) {
		index[initial_state->id] = true;
	}

	// Setup references
	d_to_n->push_back(&initial_states);
	n_to_d[index] = work.front();

	// Used to create the d_to_n vector;
	std::vector<NFAState*>* accumulator;

	while (!work.empty()) {
		// Get DFAState to be considered
		DFAState* d_state = work.front();
		work.pop();

		for (auto letter : *alphabet) {
			std::fill(index.begin(), index.end(), false);
			// Kept to pass to the d_to_n vector
			accumulator = new std::vector<NFAState*>;
			
			// If any of the NFAStates is final, so is the DFAState
			bool is_final = false;
			for (auto n_state : *(d_to_n->at(d_state->id))) {
				for (auto n_state_c : n_state->move(letter)) {
					// add_to_acc allows O(1) prevention of duplicates
					// in the accumulator vector
					// TODO: Remove accumulator and use index and mask instead
					if (!n_state_c->added_to_acc) {
						index[n_state_c->id] = true;
						accumulator->push_back(n_state_c);
						n_state_c->added_to_acc = true;
						is_final = n_state_c->accepting ? true : is_final;
					}
				}
				for (auto acc : *accumulator) {
					acc->added_to_acc = false;
				}
			}
			// Target is the newly collected set of NFAStates
			DFAState* target = n_to_d[index];

			// Doesn't exist yet
			if (target == nullptr) {
				target = new DFAState(DFA_id_counter, alphabet->size());
				DFA_id_counter++;
				d_to_n->push_back(accumulator);
				n_to_d[index] = target;
				// If it already exists, it has to already
				// be in the queue, or at least have been
				// in the queue
				work.push(target);

				// Update result
				if (is_final) {
					result->final_states.push_back(target);
					target->accepting = true;
				}
				result->states.push_back(target);
			} else {
				// Clean up some memory
				delete accumulator;
			}
			
			// Add transition
			d_state->transitions.at(letter) = target;
		}
	}
	// Finally return result
	return result;
}

std::ostream& operator<<(std::ostream& os, const NFA& nfa) {
	for (auto state : nfa.states) {
		for (auto trans: state->transitions) {
				os << (state->initial ? ">" : "")
					<< state->id << " --(" << trans.second << ")-> " << trans.first->id
					<< (trans.first->accepting ? "*" : "") << std::endl;
		}
	}
	return os;
}
