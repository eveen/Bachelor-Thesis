#include "DFA.hpp"

DFAState::DFAState(unsigned int id, unsigned int alphabet_size):transitions(alphabet_size) {
	this->id = id;
}

std::ostream& operator<<(std::ostream& os, const DFA& dfa) {
	for (auto state : dfa.states) {
		for (unsigned int i = 0; i < state->transitions.size(); i++) {
			if (state->transitions[i] != nullptr) {
				os << (state->initial ? ">" : "")
					<< state->id << " --(" << i << ")-> " << state->transitions[i]->id
					<< (state->transitions[i]->accepting ? "*" : "") << std::endl;
			}
		}
	}
	return os;
}

NFA* DFA::reverse() {
	unsigned int a_size = states[0]->transitions.size();
	NFA* result = new NFA(states.size(), a_size);

	NFAState* n_state_from;
	NFAState* n_state_to;

	result->final_states.push_back(new NFAState(initial_state->id));
	result->final_states.back()->accepting = true;
	result->states[initial_state->id] = result->final_states.back();

	NFAState* fin;
	for (DFAState* d_state : final_states) {
		if (result->states[d_state->id] == nullptr) {
			fin = new NFAState(d_state->id);
			result->states[d_state->id] = fin;
		} else {
			fin = result->states[d_state->id];
		}
		fin->initial = true;
		result->initial_states.push_back(fin);
	}

	for (DFAState* d_state : states) {
		for (unsigned int i = 0; i < d_state->transitions.size(); i++) {
			if (result->states.at(d_state->id) == nullptr) {
				n_state_from = new NFAState(d_state->id);
				result->states.at(d_state->id) = n_state_from;
			} else {
				n_state_from = result->states.at(d_state->id);
			}
			if (result->states.at(d_state->transitions[i]->id) == nullptr) {
				n_state_to = new NFAState(d_state->transitions[i]->id);
				result->states.at(d_state->transitions[i]->id) = n_state_to;
			} else {
				n_state_to = result->states.at(d_state->transitions[i]->id);
			}
			n_state_to->transitions.push_back(std::make_pair(n_state_from, i));
		}
	}

	return result;
}
