#include "minimization.hpp"

const char *argp_program_version = "Minimization 1.0";

static char doc[] =
	"An implementation of three automata mimizing algorithms";

const char *argp_program_bug_address = "<mail@erinvanderveen.nl>";

static char args_doc[] =
	"AUTOMATON";

static struct argp_option options[] = {
	{"hopcroft", 'h', NULL, 0, "Use Hopcroft's algorithm"},
	{"brzozowski", 'b', NULL, 0, "Use Brzozowski's algorithm"},
	{"watson", 'w', NULL, 0, "Use Watson's algorithm"},
	//{"input", 'i', "AUTOMATON", 0, "Input the given timuk file"},
	{0}
};

struct arguments
{
	char* input_filename;
	bool hopcroft;
	bool brzozowski;
	bool watson;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	struct arguments *arguments = (struct arguments*) state->input;

	switch (key)
	{
		case 'h':
			arguments->hopcroft = 1;
			break;
		case 'b':
			arguments->brzozowski = 1;
			break;
		case 'w':
			arguments->watson = 1;
		case ARGP_KEY_ARG:
			if (state->arg_num > 1)
				argp_usage(state);

			arguments->input_filename = arg;
			break;
		case ARGP_KEY_END:
			if (state->arg_num < 1)
				argp_usage(state);
			break;
		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

int main(int argc, char** argv) {

	struct arguments arguments;
	arguments.hopcroft = false;
	arguments.brzozowski = false;
	arguments.watson = false;
	arguments.input_filename = NULL;

	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	// The input NFA
	NFA* nfa = new NFA(arguments.input_filename);

	// Convert to DFA
	DFA* dfa = nfa->determinize_memory();
	std::cout << *dfa << std::endl;

	Hopcroft h;

	h.dfa = dfa;

	clock_t start = clock();

	h.minimize();
	h.construct_dfa();

	std::cout << "In: " << (double)(clock() - start)/CLOCKS_PER_SEC 
	<< " seconds" << std::endl;
	std::cout << *h.dfa << std::endl;

	return 0;
}
